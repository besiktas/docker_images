from flask import Flask, render_template

app = Flask(__name__)


@app.route("/", subdomain="balca")
def balca():
    return "I love you balca"


@app.route("/", subdomain="test")
def test():
    return "test route"


@app.route("/")
def main():
    return "Nothing"
    # return render_template("whale_hello.html")


if __name__ == "__main__":
    app.run(debug=True, host="erdos.space")

